<?php

/**
 * @file
 * Adds token support for locale_currency.
 */

/**
 * Implements hook_token_info().
 */
function locale_currency_token_info() {
  $site['locale_currency'] = array(
    'name' => t("Locale currency"),
    'description' => t("Currency code associated with current language."),
  );

  return array(
    'tokens' => array(
      'site' => $site,
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function locale_currency_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  if ($type == 'site') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'locale_currency':
          $locale_currency = locale_currency_get_current_currency();
          $replacements[$original] = check_plain($locale_currency);
          break;
      }
    }
  }

  return $replacements;
}
